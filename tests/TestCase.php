<?php

namespace avayabaniya\CompareMicroserviceAndVendor\Tests;

use avayabaniya\CompareMicroserviceAndVendor\CompareMicroserviceAndVendorServiceProvider;

class TestCase extends \Orchestra\Testbench\TestCase {

    public function setUp(): void
    {
        parent::setUp();
        // additional setup
    }

    protected function getPackageProviders($app)
    {
        return [
            CompareMicroserviceAndVendorServiceProvider::class,
        ];
    }

    protected function getEnvironmentSetUp($app)
    {
        // perform environment setup
    }
}
