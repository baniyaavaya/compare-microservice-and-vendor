# Laravel Compare Microservice and Vendor
The package preconfigures the boilerplate code with the feature to check transaction status between vendor through api and microservice through database. Each compared records are saved in recheck_transactions table.

## Installation
You can install this package via composer using this command:
```
composer require avayabaniya/compare-microservice-and-vendor
```
The package will automatically register itself for laravel projects.

For Lumen projects the package needs to be manually registered by adding the following line in app.php file.
```
$app->register(\avayabaniya\CompareMicroserviceAndVendor\CompareMicroserviceAndVendorServiceProvider::class);
```


## How to use
Firstly, run the migration to use recheck_transactions table
```
php artisan migrate
```
This will create `repost_transactions` table in your database where each recheck is saved using `RecheckTransaction` eloquent model.

Next step will be to create required classes, using the following artisan command, to implement the feature of the package.
```
php artisan make:compare Wallet/Compare/DummyTransaction 
```
This will create 3 classes
- `DummyTransactionApiCompare` class implementing `VendorApiStatusContract` interface in `Wallet/Compare` directory which should be used to implement api checks



- `DummyTransactionDbCompare` class implementing `MicroserviceDbStatusContract` interface in `Wallet/Compare` directory which should be used to implement database checks


- `DummyTransactionCronCompare` class in `Wallet/Compare/Cron` directory which can be used to implement cron task


