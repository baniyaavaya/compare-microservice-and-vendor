<?php

namespace avayabaniya\CompareMicroserviceAndVendor\Compare\Contracts;

interface MicroserviceDbStatusContract
{
    //currentStatus
    public function getCurrentStatus() : string;

    public function setCurrentStatus(string $status) : MicroserviceDbStatusContract;

    //check status
    public function checkStatus(string $preTransactionId);
}