<?php

namespace avayabaniya\CompareMicroserviceAndVendor\Compare\Contracts;

interface VendorApiStatusContract
{
    //currentStatus
    public function getCurrentStatus() : string;

    public function setCurrentStatus(string $status) : VendorApiStatusContract;

    //jsonRequest
    public function getJsonRequest() : array;

    public function setJsonRequest(array $jsonRequest) : VendorApiStatusContract;

    //jsonResponse
    public function getJsonResponse() : array;

    public function setJsonResponse(array $jsonResponse) : VendorApiStatusContract;


    //check status
    public function checkStatus(string $preTransactionId);



}