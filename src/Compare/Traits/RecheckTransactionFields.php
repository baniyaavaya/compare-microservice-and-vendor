<?php

namespace avayabaniya\CompareMicroserviceAndVendor\Compare\Traits;

use avayabaniya\CompareMicroserviceAndVendor\Compare\CompareApiAndDb;

trait RecheckTransactionFields
{
    protected string $vendorId;
    protected int $userId;
    protected string $recheckTransactionType;

    protected string $special1;
    protected string $special2;
    protected string $special3;
    protected string $special4;
    protected string $text1;
    protected string $text2;

    /**
     * @return string
     */
    public function getVendorId(): string
    {
        return $this->vendorId;
    }

    /**
     * @param string $vendorId
     * @return CompareApiAndDb
     */
    public function setVendorId(string $vendorId): CompareApiAndDb
    {
        $this->vendorId = $vendorId;
        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return CompareApiAndDb
     */
    public function setUserId(int $userId): CompareApiAndDb
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @return string
     */
    public function getRecheckTransactionType(): string
    {
        return $this->recheckTransactionType;
    }

    /**
     * @param string $recheckTransactionType
     * @return CompareApiAndDb
     */
    public function setRecheckTransactionType(string $recheckTransactionType): CompareApiAndDb
    {
        $this->recheckTransactionType = $recheckTransactionType;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecial1(): string
    {
        return $this->special1;
    }

    /**
     * @param string $special1
     * @return CompareApiAndDb
     */
    public function setSpecial1(string $special1): CompareApiAndDb
    {
        $this->special1 = $special1;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecial2(): string
    {
        return $this->special2;
    }

    /**
     * @param string $special2
     * @return CompareApiAndDb
     */
    public function setSpecial2(string $special2): CompareApiAndDb
    {
        $this->special2 = $special2;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecial3(): string
    {
        return $this->special3;
    }

    /**
     * @param string $special3
     * @return CompareApiAndDb
     */
    public function setSpecial3(string $special3): CompareApiAndDb
    {
        $this->special3 = $special3;
        return $this;
    }

    /**
     * @return string
     */
    public function getSpecial4(): string
    {
        return $this->special4;
    }

    /**
     * @param string $special4
     * @return CompareApiAndDb
     */
    public function setSpecial4(string $special4): CompareApiAndDb
    {
        $this->special4 = $special4;
        return $this;
    }

    /**
     * @return string
     */
    public function getText1(): string
    {
        return $this->text1;
    }

    /**
     * @param string $text1
     * @return CompareApiAndDb
     */
    public function setText1(string $text1): CompareApiAndDb
    {
        $this->text1 = $text1;
        return $this;
    }

    /**
     * @return string
     */
    public function getText2(): string
    {
        return $this->text2;
    }

    /**
     * @param string $text2
     * @return CompareApiAndDb
     */
    public function setText2(string $text2): CompareApiAndDb
    {
        $this->text2 = $text2;
        return $this;
    }



}