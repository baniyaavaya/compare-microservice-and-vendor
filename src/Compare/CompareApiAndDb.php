<?php

namespace avayabaniya\CompareMicroserviceAndVendor\Compare;

use avayabaniya\CompareMicroserviceAndVendor\Compare\Contracts\MicroserviceDbStatusContract;
use avayabaniya\CompareMicroserviceAndVendor\Compare\Contracts\VendorApiStatusContract;
use avayabaniya\CompareMicroserviceAndVendor\Compare\Traits\RecheckTransactionFields;
use avayabaniya\CompareMicroserviceAndVendor\Models\RecheckTransaction;
use Illuminate\Database\Eloquent\Model;

class CompareApiAndDb
{
    use RecheckTransactionFields;

    protected string $preTransactionId;

    protected VendorApiStatusContract $apiStatus;

    protected MicroserviceDbStatusContract $dbStatus;

    public function __construct(string $preTransactionId, VendorApiStatusContract $apiStatus, MicroserviceDbStatusContract $dbStatus)
    {
        $this->preTransactionId = $preTransactionId;
        $this->apiStatus = $apiStatus;
        $this->dbStatus = $dbStatus;
    }

    public function compare()
    {
        $this->dbStatus->checkStatus($this->preTransactionId);
        $this->apiStatus->checkStatus($this->preTransactionId);

        return $recheck = RecheckTransaction::create([
            "pre_transaction_id" => $this->preTransactionId,
            "vendor_id" => $this->vendorId,
            "user_id" => $this->userId ?? null,
            "microservice_status" => $this->dbStatus->getCurrentStatus(),
            "vendor_status" => $this->apiStatus->getCurrentStatus(),
            "recheck_transaction_type" => $this->recheckTransactionType,
            "special1" => $this->special1 ?? null,
            "special2" => $this->special2 ?? null,
            "special3" => $this->special3 ?? null,
            "special4" => $this->special4 ?? null,
            "text1" => $this->text1 ?? null,
            "text2" => $this->text2 ?? null,
            "json_request" => json_encode($this->apiStatus->getJsonRequest()),
            "json_response" => json_encode($this->apiStatus->getJsonResponse())
        ]);

    }
}