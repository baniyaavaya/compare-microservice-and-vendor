<?php

namespace avayabaniya\CompareMicroserviceAndVendor;

use avayabaniya\CompareMicroserviceAndVendor\Console\Commands\ApiCompareMakeCommand;
use avayabaniya\CompareMicroserviceAndVendor\Console\Commands\CompareMakeCommand;
use avayabaniya\CompareMicroserviceAndVendor\Console\Commands\CronCompareMakeCommand;
use avayabaniya\CompareMicroserviceAndVendor\Console\Commands\DbCompareMakeCommand;
use Carbon\Laravel\ServiceProvider;

class CompareMicroserviceAndVendorServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/../config/config.php', 'compareMicroserviceAndVendor');
    }

    public function boot()
    {
        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        if ($this->app->runningInConsole()) {

            $this->publishes([
                __DIR__.'/../config/config.php' => base_path('/config/compareMicroserviceAndVendor.php')
            ], 'config');

            $this->commands([
                CompareMakeCommand::class,
                DbCompareMakeCommand::class,
                ApiCompareMakeCommand::class,
                CronCompareMakeCommand::class
            ]);
        }
    }

}