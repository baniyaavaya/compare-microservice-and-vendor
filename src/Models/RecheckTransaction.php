<?php

namespace avayabaniya\CompareMicroserviceAndVendor\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RecheckTransaction extends Model
{
    use HasFactory;

    protected $guarded = [];

    CONST STATUS_FAILED = 'FAILED';
    CONST STATUS_COMPLETED = 'COMPLETED';
    CONST STATUS_PROCESSING = 'PROCESSING';
}