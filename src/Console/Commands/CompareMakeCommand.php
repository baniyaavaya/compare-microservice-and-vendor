<?php

namespace avayabaniya\CompareMicroserviceAndVendor\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Console\GeneratorCommand;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Str;

class CompareMakeCommand extends Command
{
    /**
     * The filesystem instance.
     *
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $files;

    /**
     * The console command name.
     *
     * @var string
     */
    //protected $name = 'make:compare';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:compare {name}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new Microservice and Api compare classes';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Compare';


    public function __construct(Filesystem $files)
    {
        parent::__construct();
        $this->files = $files;
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {

        $name = Str::studly($this->argument('name'));

        //db compare
        $this->call('make:dbcompare', [
            'name' => "{$name}DbCompare"
        ]);

        //api compare
        $this->call('make:apicompare', [
            'name' => "{$name}ApiCompare"
        ]);

        //cron
        $nameArr = explode("/", $name);
        $cronName = "";
        foreach ($nameArr as $key => $value) {
            if ($key == count($nameArr) - 1) {
                $value = "Cron/${value}";
            }
            $cronName = $cronName ? $cronName . "/" . $value : $value;
        }

        $this->call('make:croncompare', [
            'name' => "{$cronName}CronCompare"
        ]);
    }
}