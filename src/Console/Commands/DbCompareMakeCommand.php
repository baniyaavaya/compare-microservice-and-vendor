<?php

namespace avayabaniya\CompareMicroserviceAndVendor\Console\Commands;

use Illuminate\Console\GeneratorCommand;
use Illuminate\Support\Str;

class DbCompareMakeCommand extends GeneratorCommand
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'make:dbcompare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create new Microservice compare classes';

    /**
     * The type of class being generated.
     *
     * @var string
     */
    protected $type = 'Compare';

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        if (parent::handle() === false && ! $this->option('force')) {
            return false;
        }
    }

    protected function getStub()
    {
        return $this->resolveStubPath('/../stubs/MicroserviceCompareStatus.stub');
    }

    /**
     * Resolve the fully-qualified path to the stub.
     *
     * @param  string  $stub
     * @return string
     */
    protected function resolveStubPath($stub)
    {
        return file_exists($customPath = $this->laravel->basePath(trim($stub, '/')))
            ? $customPath
            : __DIR__.$stub;
    }
}