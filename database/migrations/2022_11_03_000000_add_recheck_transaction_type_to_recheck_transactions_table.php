<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRecheckTransactionTypeToRecheckTransactionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable('recheck_transactions')) {
            if (!Schema::hasColumn('recheck_transactions', 'recheck_transaction_type')) {
                Schema::table("recheck_transactions", function (Blueprint $table) {
                    $table->string("recheck_transaction_type")->nullable();
                });
            }
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }


};