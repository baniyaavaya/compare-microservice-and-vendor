<?php


use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRecheckTransactionsTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (! Schema::hasTable('recheck_transactions')) {
            Schema::create('recheck_transactions', function (Blueprint $table) {
                $table->id();
                $table->string('pre_transaction_id');
                $table->string('vendor_id');
                $table->unsignedBigInteger('user_id')->nullable();
                $table->string('previous_microservice_status')->nullable();
                $table->string('previous_vendor_status')->nullable();
                $table->string('microservice_status');
                $table->string('vendor_status');
                $table->string('recheck_transaction_type')->nullable();
                $table->text('json_request')->nullable();
                $table->text('json_response')->nullable();
                $table->string('special1')->nullable();
                $table->string('special2')->nullable();
                $table->string('special3')->nullable();
                $table->string('special4')->nullable();
                $table->text('text1')->nullable();
                $table->text('text2')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recheck_transactions');
    }


};